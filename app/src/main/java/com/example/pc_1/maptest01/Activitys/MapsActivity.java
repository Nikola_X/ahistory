package com.example.pc_1.maptest01.Activitys;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.example.pc_1.maptest01.Data.Coord;
import com.example.pc_1.maptest01.GoogleMaps.Markers;
import com.example.pc_1.maptest01.GoogleMaps.PolyUpdateTask;
import com.example.pc_1.maptest01.Network.FetchData;
import com.example.pc_1.maptest01.Network.LocationProvider;
import com.example.pc_1.maptest01.Network.Url;
import com.example.pc_1.maptest01.Network.Wifichecker;
import com.example.pc_1.maptest01.Permissions.Checker;
import com.example.pc_1.maptest01.R;
import com.example.pc_1.maptest01.Dialongs.ViewDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnPolylineClickListener,
        GoogleMap.OnPolygonClickListener,
        GoogleMap.OnMarkerClickListener {

    LatLng tt;
    Location mCurrent;
    LatLng def;
    LatLng a;
    private static final String TAG = "RADI";
    GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;

    Location desti;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    LocationManager mManger;
    Activity act;

    PolylineOptions lineOptions;
    ArrayList<LatLng> MarkerPoints;
    Polyline line;
    Polyline line2;
    TextView tx;
    ImageButton btn;

    private String km;


    private LatLng latLng;
    private float[] result = new float[0];
    private int test;
    boolean promena;


    boolean mMove=true;
    Coord cor;
    Checker check;
    Url ul;
    Wifichecker wifi;
    Markers mark;
    private LocationProvider LocationProv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        check= new Checker();
        act=this;
        try {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                check.checkLocationPermission(this);
            }
            ul=new Url();
            wifi=new Wifichecker();
            cor = new Coord();
            check.chckCamera(this);
            isLocationEnabled(this,MapsActivity.this);
            // Initializing
            checkWifiOnAndConnected(MapsActivity.this);

            //start location finder


            MarkerPoints = new ArrayList<>();
            //distance
            tx = (TextView) findViewById(R.id.text1);
            btn = (ImageButton) findViewById(R.id.btn);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            LocationProv = new  LocationProvider(this, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    try {
                        Log.d(TAG, "CHANGE ");
                        //current location
                        if (mLastLocation == null && mCurrent != null) {
                            mLastLocation = mCurrent;
                        }
                        mCurrent = location;
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        //Move the camera to the user's location and zoom in!

                        if( mMap!=null&&mMove)
                        {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
                            Log.d(TAG, "USER ZOOM");
                            mMove=false;

                        }

                        float dis;
                        float distance = 0;
                        if (mCurrent != null && mLastLocation != null)
                            distance = mCurrent.distanceTo(mLastLocation);
                        if (mCurrent != null && desti != null) {
                            dis = mCurrent.distanceTo(desti);
                            if (dis < 20) {
                                btn.setVisibility(View.VISIBLE);
                            }
                        }

                        LatLng origin = latLng;
                        LatLng dest = def;
                        if (distance > 5 && def != null&&checkWifiOnAndConnected(act)&&def!=a) {
                            // Getting URL to the Google Directions API
                            mLastLocation=null;
                            String url = ul.getUrl(origin,dest);
                            updateUrl(url);

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });

            def = new LatLng(0, 0);
            a = def;
            mManger = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            btn.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {

            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            mMap.getUiSettings().setMapToolbarEnabled(false);
            //Initialize Google Play Services
            buildGoogleApiClient();
            check.checkLocationPermission(this);
            mMap.setMyLocationEnabled(true);
            mark=new Markers(mMap);
            //marker click
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    if (mCurrent != null)
                        desti = new Location(mCurrent);
                    def=mark.getCoor(marker);

                    if (line != null) {
                        line.remove();
                    }

                    if (mCurrent != null) {
                        desti.setLatitude(def.latitude);
                        desti.setLongitude(def.longitude);
                        if (MarkerPoints.size() == 1) {
                            MarkerPoints.remove(0);
                            MarkerPoints.add(def);
                        }
                        if (MarkerPoints.isEmpty()) {
                            MarkerPoints.add(def);
                        }

                        // Creating MarkerOptions

                        LatLng origin = latLng;
                        LatLng dest = MarkerPoints.get(0);

//                        // Start downloading json data from Google Directions API
                        String url = ul.getUrl(origin,dest);
                        updateUrl(url);
                    }
                    return false;
                }
            });
        }catch (Exception e){

            e.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {

            //set markers on map
            mark.setMarkers();

            tt = new LatLng(mCurrent.getLatitude(), mCurrent.getLongitude());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;




    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        try {

            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_LOCATION: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted. Do the
                        // contacts-related task you need to do.
                        if (ContextCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {

                            if (mGoogleApiClient == null) {
                                buildGoogleApiClient();
                            }
                            mMap.setMyLocationEnabled(true);
                        }

                    } else {

                        // Permission denied, Disable the functionality that depends on this permission.
                        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                    return;
                }

                // other 'case' lines to check for other permissions this app might request.
                // You can add here other case statements according to your requirement.
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPolygonClick(Polygon polygon) {

    }

    @Override
    public void onPolylineClick(Polyline polyline) {

    }


    private void updateUrl(String... url) {
        Log.d("TEST2", mMap.toString());
        FetchData data = (FetchData) new FetchData(new FetchData.UrlResponse() {
            @Override
            public void processFinish(String response) {
                PolyUpdateTask task = (PolyUpdateTask) new PolyUpdateTask(new PolyUpdateTask.PolyResponse() {
                    @Override
                    public void processFinish(List<List<HashMap<String, String>>> result) {
                        ArrayList<LatLng> points;
                        lineOptions = null;

                        try {


                            // Traversing through all the routes
                            for (int i = 0; i < result.size(); i++) {
                                points = new ArrayList<>();
                                lineOptions = new PolylineOptions();

                                // Fetching i-th route
                                List<HashMap<String, String>> path = result.get(i);

                                // Fetching all the points in i-th route
                                for (int j = 0; j < path.size(); j++) {
                                    HashMap<String, String> point = path.get(j);

                                    if (j == 0) {    // Get distance from the list
                                        km = point.get("distance");
                                        continue;
                                    }

                                    double lat = Double.parseDouble(point.get("lat"));
                                    double lng = Double.parseDouble(point.get("lng"));
                                    LatLng position = new LatLng(lat, lng);

                                    points.add(position);
                                }

                                // Adding all the points in the route to LineOptions
                                lineOptions.addAll(points);
                                lineOptions.width(10);
                                lineOptions.color(Color.BLUE);

                                Log.d("onPostExecute", "onPostExecute lineoptions decoded");
                            }
                            if (mCurrent != null&&mMove) {

                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tt, 14));
                                mMove=false;
                            }
                            // Drawing polyline in the Google Map for the i-th route
                            //flickering fix
                            if (lineOptions != null && promena) {
                                tx.setText(km);
                                line = mMap.addPolyline(lineOptions);
                                Log.d(TAG, "LINE1");
                                if (line2 != null)
                                    line2.remove();
                                promena = false;
                            } else if (lineOptions != null && !promena) {
                                tx.setText(km);
                                if (line != null)
                                    line.remove();
                                line2 = mMap.addPolyline(lineOptions);
                                Log.d(TAG, "LINE2");
                                promena = true;
                            } else {
                                Log.d(TAG, "without Polylines drawn");
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }).execute(response);
            }
        }).execute(url);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    public void onClick(View view) {
        Double l1=def.latitude;
        Double l2=def.longitude;

        int mId= cor.sendId(def);
        String MiD=Integer.toString(mId);
        String coord1 = l1.toString();
        String coord2 = l2.toString();

        Intent mInt = new Intent(this, Camera.class);
        mInt.putExtra("ID", MiD);
        mInt.putExtra("lat", coord1);
        mInt.putExtra("lng", coord2);
        startActivity(mInt);
    }

    @Override
    protected void onPause() {
        try {
            super.onPause();
            LocationProv.onPause();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationProv.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            LocationProv.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void isLocationEnabled(final Context context,Activity act) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            Log.d(TAG, "gp "+gps_enabled);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.d(TAG, " "+network_enabled);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!gps_enabled && !network_enabled) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(act, "Location is turned off",1);
            // notify user

        }

    }
    public boolean checkWifiOnAndConnected(Activity act) {
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

            if( wifiInfo.getNetworkId() == -1 ){
                ViewDialog alert = new ViewDialog();
                alert.showDialog(act, "No Internet connection",2);
                return false; // Not connected to an access point
            }
            return true; // Connected to an access point
        }
        else {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(act, "No Internet connection",2);
            return false; // Wi-Fi adapter is OFF
        }
    }

}
