package com.example.pc_1.maptest01.Network;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.example.pc_1.maptest01.Dialongs.ViewDialog;

public class Wifichecker {
    public boolean checkWifiOnAndConnected(Activity act) {
        WifiManager wifiMgr = (WifiManager) act.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

            if( wifiInfo.getNetworkId() == -1 ){
                ViewDialog alert = new ViewDialog();
                alert.showDialog(act, "No Internet connection",2);
                return false; // Not connected to an access point
            }
            return true; // Connected to an access point
        }
        else {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(act, "No Internet connection",2);
            return false; // Wi-Fi adapter is OFF
        }
    }
}
