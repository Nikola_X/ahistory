package com.example.pc_1.maptest01.Network;

import android.os.AsyncTask;

public class FetchData extends AsyncTask<String, Void, String> {
    Url ul;
    Wifichecker wifi;



    public interface UrlResponse{
        void processFinish(String response);
    }
    public UrlResponse delegate= null;

    public FetchData(UrlResponse delegate){
        this.delegate=delegate;
        ul=new Url();
        wifi=new Wifichecker();
    }
    @Override
    protected String doInBackground(String... url) {

        // For storing data from web service
        String data = "";

        try {
            // Fetching the data from web service
            data = ul.downloadUrl(url[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}
