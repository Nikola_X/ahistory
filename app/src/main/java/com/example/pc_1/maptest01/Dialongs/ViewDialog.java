package com.example.pc_1.maptest01.Dialongs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.pc_1.maptest01.R;

/**
 * Created by pc_1 on 06/5/2018.
 */

public class ViewDialog {
    public void showDialog(final Activity activity, String msg, final int id){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.d_button);

        final TextView title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView text = (TextView) dialog.findViewById(R.id.txt_dia);
        text.setText(msg);
        if (id==1){
            title.setText("Location settings");
        }else {
            title.setText("Internet settings");
        }
        Button dialogYes = (Button) dialog.findViewById(R.id.btn_yes);
        dialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (id==1){
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(myIntent);
                }else {
                    Intent intt = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    activity.startActivity(intt);
                }

            }
        });
        Button dialogNo = (Button) dialog.findViewById(R.id.btn_no);
        dialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
