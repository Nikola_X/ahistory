package com.example.pc_1.maptest01.Activitys;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.pc_1.maptest01.Permissions.Checker;
import com.example.pc_1.maptest01.R;

public class Index extends AppCompatActivity {

    private static int TIME_OUT = 5000;
    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Checker chk= new Checker();
        chk.chckCamera(this);
        chk.checkLocationPermission(this);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Index.this, MapsActivity.class);
                startActivity(i);
                finish();
            }
        }, TIME_OUT);
    }
}
