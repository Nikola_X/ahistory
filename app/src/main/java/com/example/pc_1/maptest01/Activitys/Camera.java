package com.example.pc_1.maptest01.Activitys;

import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.example.pc_1.maptest01.Network.LocationProvider;
import com.example.pc_1.maptest01.R;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.architect.ArchitectView;

import java.io.IOException;

public class Camera extends AppCompatActivity {

    private ArchitectView architectView;
    private LocationProvider locationProvider;
    String lat;
    String lng;


    private String[] LL;
    private String newString;
    private String mID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.pc_1.maptest01.R.layout.activity_camera);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {

            this.architectView = (ArchitectView) this.findViewById(R.id.architectView);
            final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration();
            config.setLicenseKey("Zd0d4+6QKP37bt6zoGW9uM03VXfI2pLC6pWuFO5uNeEb6be9FPFEwRsEaJB3t55ASttulvAUILhVQ3EyX6NUzFCwc8n8GC4Q99gaMvUIzVIxjhKgOYkFZmoZodm7PQBBa1dYB/CLKZXwEGketII1QTc+XHmwv2Yhw7VMq6v2b4hTYWx0ZWRfXx3ExWZFiLe9tLmxcN3y3BHKt0E/CRaOfo5n4iV+YgeGXcBRm3S3hvNYlJq4+rIEGMFTiAMYNezSq4L8v9TN5s8bRnA+Qjuztd7Du1XmH9Pkkll6CjSb5jyfmbpFziiH3is2k3L9Y3kOz86k2ZZGVQ+k2CRazYx9SI/5rOOQRxA0V+vFR0W2Cd3S5nMjU1tKD9rpMJr9HViJQG+M1PRxHCUuWWU5uG+XmouML18nMq2Q84hHP3RzB1lqjFA9btAMCvR+6OUS+DSW4w181Q2lrAANODCAcU84HAJnTVTPVgrLr9O/APyqZ8DRQB/qCITp8IanTnDeDdDY9FJkA1u5EThvBi0RkIZMWXR0NUwRzDY4bQY7GAhm0yDRutKJvC21vVsUi5WI0fT/d5zEqpLruVYttDgPpghgP4+EpeHF3Mjq+1zDn8p1itkwxXkEgwRyjijiNY1V/RNjhyoTZVc8pF3VeqOhOPZiJRO5xtR9hv15Q0FHQL7x/Y5Kmuxq3TJKYBD5cV/dr0EUtCiZuc/Y5+YMm/fZn83yQ40DyG9ehG5Khe22MWYA58M90lwH4SYbVjiKbmslRKTA");
            this.architectView.onCreate(config);

            Extras();

            this.architectView.callJavascript("locationGet("+lat+","+lng+","+mID+")");
            locationProvider = new LocationProvider(this, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location!=null && Camera.this.architectView != null ) {
                        // check if location has altitude at certain accuracy level & call right architect method (the one with altitude information)
                        if ( location.hasAltitude() && location.hasAccuracy() && location.getAccuracy()<7) {
                            Camera.this.architectView.setLocation( location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getAccuracy() );
                        } else {
                            Camera.this.architectView.setLocation( location.getLatitude(), location.getLongitude(), location.hasAccuracy() ? location.getAccuracy() : 1000 );
                        }
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }catch (Exception e){
            Log.d(TAG, e.toString());
            //save.ToFile(e);
        }


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        architectView.onPostCreate();

        try {
            this.architectView.load("file:///android_asset/demo1/index.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");

    }

    @Override
    protected void onResume() {
        super.onResume();
        architectView.onResume();
        // start location updates
        locationProvider.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        architectView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        architectView.onPause();
        // stop location updates
        locationProvider.onPause();
    }
        private static final String TAG = "RADI";


    public void Extras(){
        Intent in = getIntent();
        lat=in.getStringExtra("lat");
        lng=in.getStringExtra("lng");
        mID=in.getStringExtra("ID");
        Log.d(TAG, mID);
    }

}
