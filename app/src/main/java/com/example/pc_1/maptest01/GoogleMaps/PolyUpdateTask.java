package com.example.pc_1.maptest01.GoogleMaps;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.pc_1.maptest01.Data.DataParser;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class PolyUpdateTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
    PolylineOptions lineOptions;
    GoogleMap mMap;
    Polyline line;
    Polyline line2;
    TextView tx;
    String km;
    LatLng tt;
    boolean promena;
    boolean mMove=true;

    public interface PolyResponse{
        void processFinish(List<List<HashMap<String, String>>> response);
    }
    public PolyResponse delegate=null;
    public PolyUpdateTask(PolyResponse delegate){
        this.delegate=delegate;

    }
    //get map and current location

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

        JSONObject jObject;
        List<List<HashMap<String, String>>> routes = null;

        try {

            jObject = new JSONObject(jsonData[0]);
            Log.d("ParserTask", jsonData[0].toString());
            DataParser parser = new DataParser();
            Log.d("ParserTask", parser.toString());

            // Starts parsing data

            routes = parser.parse(jObject);
            Log.d("ParserTask", "Executing routes");
            Log.d("ParserTask", routes.toString());


        } catch (Exception e) {
            Log.d("ParserTask", e.toString());

            e.printStackTrace();
        }

        return routes;

    }

    // Executes in UI thread, after the parsing process
    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
        delegate.processFinish(result);
    }
}
