package com.example.pc_1.maptest01.GoogleMaps;

import android.util.Log;

import com.example.pc_1.maptest01.Data.Coord;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Markers {

    private Marker mtest;
    private Marker Centar;
    private Marker Gml;
    private Marker Obe;
    private Marker Narod;
    private Marker Duvan;
    private Marker Tvrdj_u;
    private Marker Sud;
    private Marker Ctvrdj;

    private GoogleMap mMap;
    private Coord cor;
    public Markers(GoogleMap mMap){
       this.mMap=mMap;
        cor = new Coord();
    }

    public void setMarkers(){
        mtest = mMap.addMarker(new MarkerOptions()
                .position(cor.testC)
                .title("VTS")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));

        Centar = mMap.addMarker(new MarkerOptions()
                .position(cor.CENTAR)
                .title("Trg Kralja milana")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Gml = mMap.addMarker(new MarkerOptions()
                .position(cor.AMBASADA)
                .title("Generala Milojka Lesjanina")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Narod = mMap.addMarker(new MarkerOptions()
                .position(cor.POZOR)
                .title("Narodno pozoriste")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Sud = mMap.addMarker(new MarkerOptions()
                .position(cor.SUD)
                .title("Sud")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Ctvrdj = mMap.addMarker(new MarkerOptions()
                .position(cor.TVRD_MOST)
                .title("Most tvrdjava")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Tvrdj_u = mMap.addMarker(new MarkerOptions()
                .position(cor.TVRDJ_UN)
                .title("Tvrdjava")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Duvan = mMap.addMarker(new MarkerOptions()
                .position(cor.DUVANSKA)
                .title("Duvanska Industrija")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
        Obe = mMap.addMarker(new MarkerOptions()
                .position(cor.OBEL)
                .title("Obelicev Venac")
                .icon(BitmapDescriptorFactory.defaultMarker(229)));
    }
    public LatLng getCoor(Marker marker){
        cor= new Coord();
        Log.d("MARKER", marker.getTitle());
        if (marker.equals(Centar)) {
            return cor.CENTAR;

        } else if (marker.equals(Gml)) {
            return  cor.AMBASADA;

        }else if (marker.equals(Obe)) {
            return cor.OBEL;

        }else if (marker.equals(Narod)) {
            return cor.POZOR;

        }else if (marker.equals(Duvan)) {
            return cor.DUVANSKA;

        }else if (marker.equals(Tvrdj_u)) {
            return cor.TVRDJ_UN;

        }else if (marker.equals(Sud)) {
            return cor.SUD;

        }else if (marker.equals(Ctvrdj)) {
            return cor.TVRD_MOST;

        }else{
            return cor.testC;

        }
    }

}
