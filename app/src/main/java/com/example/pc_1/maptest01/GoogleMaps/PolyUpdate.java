package com.example.pc_1.maptest01.GoogleMaps;

import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.pc_1.maptest01.Data.DataParser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PolyUpdate {
    PolylineOptions lineOptions;
    Location mCurrent;
    GoogleMap mMap;
    Polyline line;
    Polyline line2;
    TextView tx;
    String km;
    LatLng tt;
    boolean promena;
    boolean mMove=true;

    public void loc(Location mCurrent, GoogleMap mMap){
        this.mCurrent=mCurrent;
        this.mMap=mMap;
    }

    public void Task(String result){
        ParserTask task = new ParserTask();
        task.execute(result);
    }
      private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {

                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data

                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());


            } catch (Exception e) {
                Log.d("ParserTask", e.toString());

                e.printStackTrace();
            }

            return routes;

        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            lineOptions = null;
            try {


                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        if (j == 0) {    // Get distance from the list
                            km = point.get("distance");
                            continue;
                        }

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(Color.BLUE);

                    Log.d("onPostExecute", "onPostExecute lineoptions decoded");
                }
                if (mCurrent != null&&mMove) {

                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tt, 14));
                    mMove=false;
                }
                // Drawing polyline in the Google Map for the i-th route
                //flickering fix
                if (lineOptions != null && promena) {
                    tx.setText(km);
                    line = mMap.addPolyline(lineOptions);
                    if (line2 != null)
                        line2.remove();
                    promena = false;
                } else if (lineOptions != null && !promena) {
                    tx.setText(km);
                    if (line != null)
                        line.remove();
                    line2 = mMap.addPolyline(lineOptions);
                    promena = true;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}