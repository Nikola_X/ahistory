package com.example.pc_1.maptest01.Data;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by pc_1 on 06/5/2018.
 */

public class Coord {
    public final  LatLng testC=new LatLng(43.329799, 21.889530);
    public final  LatLng CENTAR=new LatLng(43.321225, 21.895753);
    public final LatLng AMBASADA=new LatLng(43.320104, 21.891215);
    public final LatLng TVRDJ_UN=new LatLng(43.321225, 21.895753);
    public final LatLng TVRD_MOST=new LatLng(43.323389, 21.895218);
    public final LatLng SUD=new LatLng(43.320711, 21.900482);
    public final LatLng DUVANSKA=new LatLng(43.333856, 21.881729);
    public final LatLng POZOR=new LatLng(43.320325, 21.899939);
    public final LatLng OBEL=new LatLng(43.315928, 21.891449);


    public int sendId(LatLng a){
        if (a==CENTAR){
            return 0;
        }else if (a==AMBASADA){
            return 1;
        }else if (a==TVRDJ_UN){
            return 2;
        }else if (a==OBEL){
            return 3;
        }else if (a==TVRD_MOST){
            return 4;
        }else if (a==SUD){
            return 5;
        }else if (a==DUVANSKA){
            return 6;
        }else if (a==POZOR){
            return 7;
        }else {
            return 9;
        }

    }
}
