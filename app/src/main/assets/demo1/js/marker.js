        var added = false;  // Flag to hold whether markers have been added
        var poiOne;     // Reference to the first poin of interest
        var poiTwo;     // Reference to the second point of interest
        var loc1;
        var mId;
        var iId=0;
        var markerList = [];
        var marker_image;
        var marker_loc2;

        function locationGet(lat,lng,mtest){
            markerList[0]=lat;
            markerList[1]=lng;
            if(mtest <8){
                mId=mtest;
            }

           AR.logger.debug(lat);
        }
        function setImg(){

        }
        function getImg(){
               poiTwo.destroy();
               mId++;
               if(mId>7){
                    mId=0;
               }
                AR.logger.debug("markers d");
                marker_image =new AR.ImageResource(loc[mId]);
                 marker_drawable2=new AR.ImageDrawable(marker_image, 3,{
                 onClick: function(){
                        getImg();
                    }
                 })

                poiTwo = new AR.GeoObject(marker_loc2, {
                                    drawables: {
                                        cam: [marker_drawable2]
                                    }
                                });
         }
        // Called when location changes
        function locationChanged(lat, lon, alt, acc){

            // First time the function runs wer add the markers
            if (!added){

                // ------ First Marker ----------

                // create the image for the marker
//                var marker_image = new AR.ImageResource("assets/test.jpg");
//
//                // create the marker location
//                var marker_loc = new AR.GeoLocation(lat+0.000688, lon+0.001905, alt);
//
//                // create the drawable (set display parameters of the image - just size here)
//                var marker_drawable = new AR.ImageDrawable(marker_image, 5);
//
//                // create GeoObject (also adds to the display)
//                poiOne = new AR.GeoObject(marker_loc, {
//                    drawables: {
//                        cam: [marker_drawable]
//                    }
//                });

                // ------ Second Marker ----------

                // create the image for the marker
                marker_image =new AR.ImageResource(loc[mId]);

                // create the marker location
                 marker_loc2= new AR.GeoLocation(markerList[0], markerList[1], alt);

                 //var marker_loc2 = new AR.GeoLocation(dt1+0.002699,dt2+0.002294, alt);
                // create the drawable (set display parameters of the image - just size here)
                var marker_drawable2 = new AR.ImageDrawable(marker_image, 3,{
                onClick : function(){

                        getImg();

                    }
                });

                // create GeoObject (also adds to the display)
//                if(marker_loc2.distanceTo(loc1)==50){
                poiTwo = new AR.GeoObject(marker_loc2, {
                    drawables: {
                        cam: [marker_drawable2]
                    }
                });
//                }
                added = true;   // Store that markers were added

                 AR.logger.debug("markers added");
            }
            else {
//                // Each other time position is changed we update
//                //  location is stored in an array (not sure why)
//
////                poiOne.locations[0].latitude = lat+0.000688;
////                poiOne.locations[0].longitude = lon+0.001905;
////                poiOne.locations[0].altitude = alt;
//
                   poiTwo.locations[0].latitude = markerList[0];
                   poiTwo.locations[0].longitude = markerList[1];
                   poiTwo.locations[0].altitude = alt;

               AR.logger.debug("markers updated");
            }
        }


      AR.logger.debug("starting");
//      AR.logger.activateDebugMode ();

        // Set which function is called when the location changes
        AR.context.onLocationChanged = locationChanged;
